#include <iostream>

#include "bubblesort.h"
#include "selectionsort.h"
#include "insertionsort.h"
#include "shellsort.h"
#include "heapsort.h"
#include "quicksort.h"
#include "file.h"
#include "control.h"

using namespace std;

int main()
{
    int n = 9999; //Size of vector
    int v0[n];
    int v1[n];
    int v2[n];
    int v3[n];
    int v4[n];
    int v5[n];
    int v6[n];
    int v7[n];
    int v8[n];
    int v9[n];

    File *file = new File();

    file->openFile("../Arquivos/10000/0.txt",v0);
    file->openFile("../Arquivos/10000/1.txt",v1);
    file->openFile("../Arquivos/10000/2.txt",v2);
    file->openFile("../Arquivos/10000/3.txt",v3);
    file->openFile("../Arquivos/10000/4.txt",v4);
    file->openFile("../Arquivos/10000/5.txt",v5);
    file->openFile("../Arquivos/10000/6.txt",v6);
    file->openFile("../Arquivos/10000/7.txt",v7);
    file->openFile("../Arquivos/10000/8.txt",v8);
    file->openFile("../Arquivos/10000/9.txt",v9);


/*
    // InsertionSort
    InsertionSort *ins = new InsertionSort();
    Control *controlIns = new Control();

    cout << "ins" << endl;
    cout << "Conjunto v0" << endl;
    ins->sort(v0,n,controlIns);
    cout << controlIns->getComp() << endl;
    cout << controlIns->getMov() << endl << endl;
    controlIns->setComp(0);
    controlIns->setMov(0);

    cout << "Conjunto v1" << endl;
    ins->sort(v1,n,controlIns);
    cout << controlIns->getComp() << endl;
    cout << controlIns->getMov() << endl << endl;
    controlIns->setComp(0);
    controlIns->setMov(0);

    cout << "Conjunto v2" << endl;
    ins->sort(v2,n,controlIns);
    cout << controlIns->getComp() << endl;
    cout << controlIns->getMov() << endl << endl;
    controlIns->setComp(0);
    controlIns->setMov(0);

    cout << "Conjunto v3" << endl;
    ins->sort(v3,n,controlIns);
    cout << controlIns->getComp() << endl;
    cout << controlIns->getMov() << endl << endl;
    controlIns->setComp(0);
    controlIns->setMov(0);

    cout << "Conjunto v4" << endl;
    ins->sort(v4,n,controlIns);
    cout << controlIns->getComp() << endl;
    cout << controlIns->getMov() << endl << endl;
    controlIns->setComp(0);
    controlIns->setMov(0);

    cout << "Conjunto v5" << endl;
    ins->sort(v5,n,controlIns);
    cout << controlIns->getComp() << endl;
    cout << controlIns->getMov() << endl << endl;
    controlIns->setComp(0);
    controlIns->setMov(0);

    cout << "Conjunto v6" << endl;
    ins->sort(v6,n,controlIns);
    cout << controlIns->getComp() << endl;
    cout << controlIns->getMov() << endl << endl;
    controlIns->setComp(0);
    controlIns->setMov(0);

    cout << "Conjunto v7" << endl;
    ins->sort(v7,n,controlIns);
    cout << controlIns->getComp() << endl;
    cout << controlIns->getMov() << endl << endl;
    controlIns->setComp(0);
    controlIns->setMov(0);

    cout << "Conjunto v8" << endl;
    ins->sort(v8,n,controlIns);
    cout << controlIns->getComp() << endl;
    cout << controlIns->getMov() << endl << endl;
    controlIns->setComp(0);
    controlIns->setMov(0);

    cout << "Conjunto v9" << endl;
    ins->sort(v9,n,controlIns);
    cout << controlIns->getComp() << endl;
    cout << controlIns->getMov() << endl << endl;
    controlIns->setComp(0);
    controlIns->setMov(0);


    // SelectionSort
    SelectionSort *sel = new SelectionSort();
    Control *controlSel = new Control();

    cout << "Sel" << endl;
    cout << "Conjunto v0" << endl;
    sel->sort(v0,n,controlSel);
    cout << controlSel->getComp() << endl;
    cout << controlSel->getMov() << endl << endl;
    controlSel->setComp(0);
    controlSel->setMov(0);

    cout << "Conjunto v1" << endl;
    sel->sort(v1,n,controlSel);
    cout << controlSel->getComp() << endl;
    cout << controlSel->getMov() << endl << endl;
    controlSel->setComp(0);
    controlSel->setMov(0);

    cout << "Conjunto v2" << endl;
    sel->sort(v2,n,controlSel);
    cout << controlSel->getComp() << endl;
    cout << controlSel->getMov() << endl << endl;
    controlSel->setComp(0);
    controlSel->setMov(0);

    cout << "Conjunto v3" << endl;
    sel->sort(v3,n,controlSel);
    cout << controlSel->getComp() << endl;
    cout << controlSel->getMov() << endl << endl;
    controlSel->setComp(0);
    controlSel->setMov(0);

    cout << "Conjunto v4" << endl;
    sel->sort(v4,n,controlSel);
    cout << controlSel->getComp() << endl;
    cout << controlSel->getMov() << endl << endl;
    controlSel->setComp(0);
    controlSel->setMov(0);

    cout << "Conjunto v5" << endl;
    sel->sort(v5,n,controlSel);
    cout << controlSel->getComp() << endl;
    cout << controlSel->getMov() << endl << endl;
    controlSel->setComp(0);
    controlSel->setMov(0);

    cout << "Conjunto v6" << endl;
    sel->sort(v6,n,controlSel);
    cout << controlSel->getComp() << endl;
    cout << controlSel->getMov() << endl << endl;
    controlSel->setComp(0);
    controlSel->setMov(0);

    cout << "Conjunto v7" << endl;
    sel->sort(v7,n,controlSel);
    cout << controlSel->getComp() << endl;
    cout << controlSel->getMov() << endl << endl;
    controlSel->setComp(0);
    controlSel->setMov(0);

    cout << "Conjunto v8" << endl;
    sel->sort(v8,n,controlSel);
    cout << controlSel->getComp() << endl;
    cout << controlSel->getMov() << endl << endl;
    controlSel->setComp(0);
    controlSel->setMov(0);

    cout << "Conjunto v9" << endl;
    sel->sort(v9,n,controlSel);
    cout << controlSel->getComp() << endl;
    cout << controlSel->getMov() << endl << endl;
    controlSel->setComp(0);
    controlSel->setMov(0);




    // BubbleSort
    BubbleSort *blu = new BubbleSort();
    Control *controlBlu = new Control();

    cout << "Bubble" << endl;
    cout << "Conjunto v0" << endl;
    blu->sort(v0,n,controlBlu);
    cout << controlBlu->getComp() << endl;
    cout << controlBlu->getMov() << endl << endl;
    controlBlu->setComp(0);
    controlBlu->setMov(0);

    cout << "Conjunto v1" << endl;
    blu->sort(v1,n,controlBlu);
    cout << controlBlu->getComp() << endl;
    cout << controlBlu->getMov() << endl << endl;
    controlBlu->setComp(0);
    controlBlu->setMov(0);

    cout << "Conjunto v2" << endl;
    blu->sort(v2,n,controlBlu);
    cout << controlBlu->getComp() << endl;
    cout << controlBlu->getMov() << endl << endl;
    controlBlu->setComp(0);
    controlBlu->setMov(0);

    cout << "Conjunto v3" << endl;
    blu->sort(v3,n,controlBlu);
    cout << controlBlu->getComp() << endl;
    cout << controlBlu->getMov() << endl << endl;
    controlBlu->setComp(0);
    controlBlu->setMov(0);

    cout << "Conjunto v4" << endl;
    blu->sort(v4,n,controlBlu);
    cout << controlBlu->getComp() << endl;
    cout << controlBlu->getMov() << endl << endl;
    controlBlu->setComp(0);
    controlBlu->setMov(0);

    cout << "Conjunto v5" << endl;
    blu->sort(v5,n,controlBlu);
    cout << controlBlu->getComp() << endl;
    cout << controlBlu->getMov() << endl << endl;
    controlBlu->setComp(0);
    controlBlu->setMov(0);

    cout << "Conjunto v6" << endl;
    blu->sort(v6,n,controlBlu);
    cout << controlBlu->getComp() << endl;
    cout << controlBlu->getMov() << endl << endl;
    controlBlu->setComp(0);
    controlBlu->setMov(0);

    cout << "Conjunto v7" << endl;
    blu->sort(v7,n,controlBlu);
    cout << controlBlu->getComp() << endl;
    cout << controlBlu->getMov() << endl << endl;
    controlBlu->setComp(0);
    controlBlu->setMov(0);

    cout << "Conjunto v8" << endl;
    blu->sort(v8,n,controlBlu);
    cout << controlBlu->getComp() << endl;
    cout << controlBlu->getMov() << endl << endl;
    controlBlu->setComp(0);
    controlBlu->setMov(0);

    cout << "Conjunto v9" << endl;
    blu->sort(v9,n,controlBlu);
    cout << controlBlu->getComp() << endl;
    cout << controlBlu->getMov() << endl << endl;
    controlBlu->setComp(0);
    controlBlu->setMov(0);



    // Heapsort
    HeapSort *hep = new HeapSort();
    Control *controlHep = new Control();

    cout << "hep" << endl;
    cout << "Conjunto v0" << endl;
    hep->init(v0,n,controlHep);
    cout << controlHep->getComp() << endl;
    cout << controlHep->getMov() << endl << endl;
    controlHep->setComp(0);
    controlHep->setMov(0);

    cout << "Conjunto v1" << endl;
    hep->init(v1,n,controlHep);
    cout << controlHep->getComp() << endl;
    cout << controlHep->getMov() << endl << endl;
    controlHep->setComp(0);
    controlHep->setMov(0);

    cout << "Conjunto v2" << endl;
    hep->init(v2,n,controlHep);
    cout << controlHep->getComp() << endl;
    cout << controlHep->getMov() << endl << endl;
    controlHep->setComp(0);
    controlHep->setMov(0);

    cout << "Conjunto v3" << endl;
    hep->init(v3,n,controlHep);
    cout << controlHep->getComp() << endl;
    cout << controlHep->getMov() << endl << endl;
    controlHep->setComp(0);
    controlHep->setMov(0);

    cout << "Conjunto v4" << endl;
    hep->init(v4,n,controlHep);
    cout << controlHep->getComp() << endl;
    cout << controlHep->getMov() << endl << endl;
    controlHep->setComp(0);
    controlHep->setMov(0);

    cout << "Conjunto v5" << endl;
    hep->init(v5,n,controlHep);
    cout << controlHep->getComp() << endl;
    cout << controlHep->getMov() << endl << endl;
    controlHep->setComp(0);
    controlHep->setMov(0);

    cout << "Conjunto v6" << endl;
    hep->init(v6,n,controlHep);
    cout << controlHep->getComp() << endl;
    cout << controlHep->getMov() << endl << endl;
    controlHep->setComp(0);
    controlHep->setMov(0);

    cout << "Conjunto v7" << endl;
    hep->init(v7,n,controlHep);
    cout << controlHep->getComp() << endl;
    cout << controlHep->getMov() << endl << endl;
    controlHep->setComp(0);
    controlHep->setMov(0);

    cout << "Conjunto v8" << endl;
    hep->init(v8,n,controlHep);
    cout << controlHep->getComp() << endl;
    cout << controlHep->getMov() << endl << endl;
    controlHep->setComp(0);
    controlHep->setMov(0);

    cout << "Conjunto v9" << endl;
    hep->init(v9,n,controlHep);
    cout << controlHep->getComp() << endl;
    cout << controlHep->getMov() << endl << endl;
    controlHep->setComp(0);
    controlHep->setMov(0);
*/
    // quickertioninit
    QuickSort *quick = new QuickSort();
    Control *controlQuick = new Control();

    cout << "quick" << endl;
    cout << "Conjunto v0" << endl;
    quick->init(v0,n,controlQuick);
    cout << controlQuick->getComp() << endl;
    cout << controlQuick->getMov() << endl << endl;
    controlQuick->setComp(0);
    controlQuick->setMov(0);

    cout << "Conjunto v1" << endl;
    quick->init(v1,n,controlQuick);
    cout << controlQuick->getComp() << endl;
    cout << controlQuick->getMov() << endl << endl;
    controlQuick->setComp(0);
    controlQuick->setMov(0);

    cout << "Conjunto v2" << endl;
    quick->init(v2,n,controlQuick);
    cout << controlQuick->getComp() << endl;
    cout << controlQuick->getMov() << endl << endl;
    controlQuick->setComp(0);
    controlQuick->setMov(0);

    cout << "Conjunto v3" << endl;
    quick->init(v3,n,controlQuick);
    cout << controlQuick->getComp() << endl;
    cout << controlQuick->getMov() << endl << endl;
    controlQuick->setComp(0);
    controlQuick->setMov(0);

    cout << "Conjunto v4" << endl;
    quick->init(v4,n,controlQuick);
    cout << controlQuick->getComp() << endl;
    cout << controlQuick->getMov() << endl << endl;
    controlQuick->setComp(0);
    controlQuick->setMov(0);

    cout << "Conjunto v5" << endl;
    quick->init(v5,n,controlQuick);
    cout << controlQuick->getComp() << endl;
    cout << controlQuick->getMov() << endl << endl;
    controlQuick->setComp(0);
    controlQuick->setMov(0);

    cout << "Conjunto v6" << endl;
    quick->init(v6,n,controlQuick);
    cout << controlQuick->getComp() << endl;
    cout << controlQuick->getMov() << endl << endl;
    controlQuick->setComp(0);
    controlQuick->setMov(0);

    cout << "Conjunto v7" << endl;
    quick->init(v7,n,controlQuick);
    cout << controlQuick->getComp() << endl;
    cout << controlQuick->getMov() << endl << endl;
    controlQuick->setComp(0);
    controlQuick->setMov(0);

    cout << "Conjunto v8" << endl;
    quick->init(v8,n,controlQuick);
    cout << controlQuick->getComp() << endl;
    cout << controlQuick->getMov() << endl << endl;
    controlQuick->setComp(0);
    controlQuick->setMov(0);

    cout << "Conjunto v9" << endl;
    quick->init(v9,n,controlQuick);
    cout << controlQuick->getComp() << endl;
    cout << controlQuick->getMov() << endl << endl;
    controlQuick->setComp(0);
    controlQuick->setMov(0);



//    ShellSort *she = new ShellSort();

//    Control *controlShe = new Control();

//    cout << "InsertionSort" << endl;
//    ins->sort(v,n,controlIns);
//    cout << controlIns->getComp() << endl;
//    cout << controlIns->getMov() << endl << endl;

//    cout << "BubbleSort" << endl;
//    blu->sort(v,n,controlBlu);
//    cout << controlBlu->getComp() << endl;
//    cout << controlBlu->getMov() << endl << endl;

//    cout << "ShellSort" << endl;
//    she->sort(v,n,controlShe);
//    cout << controlShe->getComp() << endl;
//    cout << controlShe->getMov() << endl << endl;

//    cout << "HeapSort" << endl;
//    hep->init(v,n,controlHep);
//    cout << controlHep->getComp() << endl;
//    cout << controlHep->getMov() << endl << endl;

//    cout << "QuickSort" << endl;
//    quick->init(v,n,controlQuick);
//    cout << controlQuick->getComp() << endl;
//    cout << controlQuick->getMov() << endl << endl;

//    cout << "SelectionSort" << endl;
//    sel->sort(v,n,controlSel);
//    cout << controlSel->getComp() << endl;
//    cout << controlSel->getMov() << endl;

    return 0;
}

