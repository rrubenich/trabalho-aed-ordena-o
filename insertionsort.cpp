#include "insertionsort.h"

InsertionSort::InsertionSort(){
}

void InsertionSort::sort(int v[], int n, Control *control){
    int cmp = 0;
    int mov = 0;

    int x, j, i;

    for(i = 1, mov++; i<n && ++cmp; i++, mov++){
        x = v[i];
        j = i -1;
        mov+=2;

        while( (j >= 0 ) && x < v[j] && ++cmp && ++cmp){
            v[j+1] = v[j];
            j--;
            mov+=2;
        }
        v[j+1] = x;
        mov++;
    }

    control->setMov(mov);
    control->setComp(cmp);
}
