#include "heapsort.h"

HeapSort::HeapSort(){
}

void HeapSort::init(int v[], int n, Control *control){
    sort(v, n, control);
}

int HeapSort::sort(int v[], int n, Control *control){
    int i = n/2;
    int dad, sun, t;

    int mov = 0;
    int cmp = 1;

    while(cmp++){
        if((cmp++) && (i>0)){
            i--;
            t = v[i];
            mov+=2;
        }else{
            n--;
            if((cmp++) && (n==0)){
                control->incMov(mov);
                control->incComp(cmp);
                return 0;
            }
            t = v[n];
            v[n] = v[0];

            mov+=3;
        }

        dad = i;
        sun = i*2;

        mov+=3;

        while((cmp++) && (sun < n)){
            if((cmp++) && (sun+1 < n) && (v[sun+1] > v[sun])){
                sun++;
                mov++;
            }
            if((cmp++) && (v[sun] > t)){
                v[dad] = v[sun];
                dad = sun;
                sun = dad*2+1;
                mov+=3;
            }
            else
                break;
        }
        v[dad] = t;
        mov++;
    }

    control->incMov(mov);
    control->incComp(cmp);
}
