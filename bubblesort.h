#ifndef BUBBLESORT_H
#define BUBBLESORT_H

#include "control.h"

class BubbleSort
{
public:
    BubbleSort();
    void sort(int v[], int n, Control *control);
};

#endif // BUBBLESORT_H
