#include "quicksort.h"
#include "uselfulmethods.h"
#include <iostream>

using namespace std;

QuickSort::QuickSort(){

}

void QuickSort::init(int v[], int n, Control *control){
    sort(v, 0, n, control);
}

void QuickSort::sort(int v[], int left, int right, Control *control){

    UselfulMethods *mAux = new UselfulMethods;

    int mov = 0;
    int cmp = 0;

    int i, j, x;
    i = left;
    j = right;

    x = v[(left+right)/2];

    mov+=3;
    while(i <= j && ++cmp){
        while((v[i] < x) && (i < right) && (cmp+=2)){
            i++;
            mov++;
        }
        while((v[j] > x) && (j > left) && (cmp+=2)){
            j--;
            mov++;
        }

        if(i <= j && ++cmp){
            mAux->swap(v, i, j);
            i++;
            j--;
            mov+=5;
        }
    }

    if(j > left && ++cmp){
        sort(v, left, j, control);
    }
    if(i < right && ++cmp){
        sort(v, i, right, control);
    }


    control->incMov(mov);
    control->incComp(cmp);
}
