#ifndef SELECTIONSORT_H
#define SELECTIONSORT_H

#include "control.h"
class SelectionSort
{
public:
    SelectionSort();
    void sort(int v[], int n, Control *control);
};

#endif // SELECTIONSORT_H
