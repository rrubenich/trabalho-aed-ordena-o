#include <iostream>

#include "bubblesort.h"
#include "uselfulmethods.h"

using namespace std;

BubbleSort::BubbleSort(){
}

void BubbleSort::sort(int v[], int n, Control *control){

    UselfulMethods *mAux = new UselfulMethods;

    int cmp = 0;
    int mov = 0;

    int i, j;

    for(i = 0, ++mov; (++cmp) && (i<n-1); i++, mov++){
        for(j = 0, mov++; j < (n-i)-1 && ++cmp; j++, mov++){
            if(v[j] > v[j+1] && cmp++){
                mAux->swap(v, j, j+1);
                mov+=3;
            }
        }
    }


    control->setMov(mov);
    control->setComp(cmp);
}
