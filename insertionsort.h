#ifndef INSERTIONSORT_H
#define INSERTIONSORT_H

#include "control.h"

class InsertionSort
{
public:
    InsertionSort();
    void sort(int v[], int n, Control *control);
};

#endif // INSERTIONSORT_H
