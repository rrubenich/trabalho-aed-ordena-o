#ifndef QUICKSORT_H
#define QUICKSORT_H

#include "control.h";

class QuickSort{
public:
    QuickSort();
    void init(int v[], int n, Control *control);
    void sort(int v[], int left, int right, Control *control);
};

#endif // QUICKSORT_H
