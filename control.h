#ifndef CONTROL_H
#define CONTROL_H

class Control{
private:
    unsigned long long int comp;
    unsigned long long int mov;

public:
    Control();

    void setComp(int value);
    void setMov(int value);
    void incComp(int value);
    void incMov(int value);

    unsigned long long int getComp();
    unsigned long long int getMov();
};

#endif // CONTROL_H
