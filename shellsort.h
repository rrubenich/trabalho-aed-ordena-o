#ifndef SHELLSORT_H
#define SHELLSORT_H

#include "control.h";

class ShellSort
{
public:
    ShellSort();
    void sort(int v[], int n, Control *control);
};

#endif // SHELLSORT_H
