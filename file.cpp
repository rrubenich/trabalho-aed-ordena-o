#include "file.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>

using namespace std;

File::File(){

}

void File::openFile(string nameFile, int *v){

    int i = 0;

    ifstream fileV;
    fileV.open(nameFile.c_str());

    if(fileV.is_open()){
        string linha;
        fileV >> linha;

        while(!fileV.eof()){

            string s = linha;
            int linhaInt = std::atoi(s.c_str());

            v[i++] = linhaInt;
            fileV >> linha;
        }
    }else{
        cout << "Arquivo inválido" << endl;
    }
}
