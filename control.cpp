#include "control.h"

Control::Control(){
    this->comp = 0;
    this->mov = 0;
}

void Control::setComp(int value){
    this->comp = value;
}

void Control::setMov(int value){
    this->mov = value;
}

void Control::incComp(int value){
    this->comp += value;
}

void Control::incMov(int value){
    this->mov += value;
}

unsigned long long int Control::getComp(){
    return comp;
}

unsigned long long int Control::getMov(){
    return mov;
}
