#include "selectionsort.h"
#include "uselfulmethods.h"

SelectionSort::SelectionSort(){

}


void SelectionSort::sort(int v[], int n, Control *control){

    UselfulMethods *mAux = new UselfulMethods;

    int cmp = 0;
    int mov = 0;
    int i, j;

    for(i = 0, mov++; i<n-1 && ++cmp; i++, mov++){
        int min = i;
        for(j = i+1, mov++; j < n && cmp++; j++, mov++){
            if(v[j] < v[min] && cmp++){
                min = j;
                mov++;
            }
        }
        if(i != min && cmp++){
            mAux->swap(v, i, min);
            mov+=3;
        }
    }

    control->setMov(mov);
    control->setComp(cmp);

}
