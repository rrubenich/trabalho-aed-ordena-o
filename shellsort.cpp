#include "shellsort.h"

ShellSort::ShellSort(){
}

void ShellSort::sort(int v[], int n, Control *control){
    int cmp = 0;
    int mov = 0;

    int x, j, h, i;

    while(h < n && ++cmp){
        h = 3*h+1;
        mov++;
    }

    while(h > 1 && ++cmp){
        h = h/3;
        mov++;
        for(i = h, mov++; i<n && cmp++; i++, mov++){
            x = v[i];
            j = i-h;
            mov+=2;

            while( (j >= 0 ) && x < v[j] && cmp++){
                v[j+h] = v[j];
                j-h;
                mov+=2;
            }
            v[j+h] = x;
            mov++;
        }
    }

    control->setMov(mov);
    control->setComp(cmp);
}
