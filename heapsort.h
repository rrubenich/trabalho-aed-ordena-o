#ifndef HEAPSORT_H
#define HEAPSORT_H

#include "control.h";

class HeapSort{
public:
    HeapSort();
    void init(int v[], int n, Control *control);
    int sort(int v[], int n, Control *control);
};

#endif // HEAPSORT_H
